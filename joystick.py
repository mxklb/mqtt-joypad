#!/usr/bin/env python
import sys
import orjson
import argparse
import time
import threading
import traceback
import gitversion.version as version
import mqttclient.mqttclient as mqttclient
from mqttclient.logger.logger import logger
from evdev import InputDevice, categorize, ecodes, list_devices
from datetime import datetime
from joyfig import Joyfig
import glob

# Configure argument parser ..
parser = argparse.ArgumentParser(
    description="Start joystick listener..",
    formatter_class=argparse.MetavarTypeHelpFormatter)
parser.add_argument('--config', type=str, default="config/", help='Path where to find joystick configs')
parser.add_argument('--broker', type=str, default="localhost", help='MQTT broker url')
parser.add_argument('--client_id', type=str, default="mqtt-joypad", help='MQTT client ID')
parser.add_argument('--topic', type=str, default="joypad", help='MQTT topic to publish to')
parser.add_argument('--skip', type=str, default="", help='Comma separated skip device names')
args = parser.parse_args()
configPath = args.config

# Prepare skip devices (containing skip string in name) ..
skipDevices = []
if(args.skip and args.skip.strip()):
    skipDevices = str(args.skip).split(',')

# Connect to the mqtt broker ..
mqttclient.setup(args.broker, args.client_id)

# Publish container version via MQTT
versionString = version.getVersion()
if len(versionString) > 0:
    versionData = { "time": datetime.timestamp(datetime.now()) * 1000 }
    versionData["service"] = "mqtt-joypad"
    versionData["version"] = versionString
    mqttclient.publish("XDMX/version", str(orjson.dumps(versionData), "utf-8"))

# Load supported device configs ..
searchpath = configPath + "*.json"
inputConfigs = [Joyfig(file) for file in glob.glob(searchpath)]
if len(inputConfigs) < 1:
    logger.error("Error: No input configs found!")
    sys.exit(1)
logger.info("Available configs:")
for config in inputConfigs:
    logger.info(" - " + str(config.name) + " = " + str(config.path))

# Dict to keep track of connected devices
captureDevices = {}

# Class to store single joystick positions
class Joystick:
    def __init__(self, right=[0,0], left=[0,0]):
        self.right = right
        self.left = left

# Set json at key = function if event corresponds 
def setJson(config, devEvent, function, jsonIn={}):
    if function not in config.config.keys():
        return jsonIn
    if devEvent.code == config.config[function]:
        jsonIn[function] = devEvent.value
    return jsonIn

# Define input capturing method ..
def inputCapturing(deviceId):
    global captureDevices
    if deviceId not in captureDevices.keys():
        logger.warning(f"Capturing loop failed device {deviceId} not defined")
        return

    device = captureDevices[deviceId]["device"]
    config = captureDevices[deviceId]["config"]
    joystick = captureDevices[deviceId]["joystick"]

    try:
        device = InputDevice(device)
        for event in device.read_loop():
            jsonData = {}
            # Buttons 
            if event.type == ecodes.EV_KEY:
                #logger.debug(str(event))
                if event.code == config.config["xBtn"]:
                    jsonData["X"] = event.value 
                if event.code == config.config["bBtn"]:
                    jsonData["B"] = event.value
                if event.code == config.config["aBtn"]:
                    jsonData["A"] = event.value
                if event.code == config.config["yBtn"]:
                    jsonData["Y"] = event.value
                if event.code == config.config["lBtn"]:
                    jsonData["L"] = event.value
                if "llBtn" in config.config.keys():
                    if event.code == config.config["llBtn"]:
                        jsonData["LL"] = event.value
                if event.code == config.config["rBtn"]:
                    jsonData["R"] = event.value
                if "rrBtn" in config.config.keys():
                    if event.code == config.config["rrBtn"]:
                        jsonData["RR"] = event.value
                if event.code == config.config["selBtn"]:
                    jsonData["select"] = event.value
                if event.code == config.config["staBtn"]:
                    jsonData["start"] = event.value
                if "joyLBtn" in config.config.keys():
                    if event.code == config.config["joyLBtn"]:
                        jsonData["left_joystick_btn"] = event.value
                if "joyRBtn" in config.config.keys():
                    if event.code == config.config["joyRBtn"]:
                        jsonData["right_joystick_btn"] = event.value
                jsonData = setJson(config, event, "up", jsonData)
                jsonData = setJson(config, event, "down", jsonData)
                jsonData = setJson(config, event, "left", jsonData)
                jsonData = setJson(config, event, "right", jsonData)
                if len(jsonData) == 0:
                    logger.info("Unknown button pressed: " + str(event))
            # Analog gamepad
            elif event.type == ecodes.EV_ABS:
                absevent = categorize(event)
                eventId = ecodes.bytype[absevent.event.type][absevent.event.code]
                eventValue = absevent.event.value
                logger.debug(str(eventId) + " " + str(eventValue))
                if "horizontal" in config.config.keys():
                    if eventId == config.config["horizontal"]:
                        jsonData["horizontal"] = eventValue
                if "vertical" in config.config.keys():
                    if eventId == config.config["vertical"]:
                        jsonData["vertical"] = eventValue
                if eventId == config.config["right_vertical"]:
                    joystick.right[0] = eventValue
                    jsonData["right_joystick"] = joystick.right
                elif eventId == config.config["right_horizontal"]:
                    joystick.right[1] = eventValue
                    jsonData["right_joystick"] = joystick.right
                elif eventId == config.config["left_vertical"]:
                    joystick.left[0] = eventValue
                    jsonData["left_joystick"] = joystick.left
                elif eventId == config.config["left_horizontal"]:
                    joystick.left[1] = eventValue
                    jsonData["left_joystick"] = joystick.left
                elif config.name == "Joystick" and not "ABS_Z" in eventId:
                    # ABS_Z is triggered often by my hama joystick!
                    logger.warning(str(eventId) + " " + str(eventValue))
            # Unknown event type
            #else:
            #     logger.warning("Unknown input event type: " + str(event.type))
            
            # Print & Publish MQTT
            if len(jsonData) > 0:
                logger.debug(str(jsonData))
                jsonData["time"] = datetime.timestamp(datetime.utcnow())
                jsonData["device"] = config.name
                jsonData["info"] = {
                    "name": device.name,
                    "info": str(device.info),
                    "uniq": device.uniq,
                    "phys": device.phys
                }
                mqttclient.publish(args.topic, str(orjson.dumps(jsonData), "utf-8"))
    except Exception as e:
        logger.warning("Capturing loop failed for device {0} -> {1}".format(config.name, str(e)))
        traceback.print_exception(*sys.exc_info())


# Start infinite listening for connected devices ..
while True:
    try:
        activeDevices = [InputDevice(path) for path in list_devices()]
        activeNames = [device.name for device in activeDevices]
        configNames = [config.name for config in inputConfigs]
        #logger.info(f"Active devices = {activeNames}")
        #logger.info(f"Active configs = {configNames}")

        validCount = 0
        captureChanged = False
        # Add configured devices (if not present)
        for deviceName in activeNames:
            valid = False
            for name in configNames:
                if name in deviceName:
                    valid = True
                    break
            if len(skipDevices) > 0:
                for skipName in skipDevices:
                    if skipName in deviceName:
                        #logger.debug(f"Skipping device {deviceName}")
                        valid = False
                        break
            if valid == False:
                continue
            validCount += 1
            activeDevice = activeDevices[activeNames.index(deviceName)]
            deviceId = name + "_" + str(validCount) + "_" + str(activeDevice.phys)
            deviceKeys = list(captureDevices.keys())
            # Add new devices (if not already in dict)
            if deviceId not in deviceKeys:
                alreadyIn = False
                for captureId in captureDevices.keys():
                   if str(activeDevice.phys) in captureId:
                       alreadyIn = (deviceName == captureDevices[captureId]["name"])
                       break
                if alreadyIn == True:
                   #logger.debug(f"Skipping {deviceId} already in list!")
                   validCount -= 1
                   continue
                captureDevices[deviceId] = { "config": inputConfigs[configNames.index(name)] }
                captureDevices[deviceId]["name"] = deviceName
                captureDevices[deviceId]["device"] = activeDevice
                captureDevices[deviceId]["uniq"] = activeDevice.uniq
                captureDevices[deviceId]["phys"] = activeDevice.phys
                captureDevices[deviceId]["info"] = activeDevice.info
                captureDevices[deviceId]["joystick"] = Joystick()
                captureDevices[deviceId]["thread"] = threading.Thread(target=inputCapturing, args=(deviceId,))
                captureDevices[deviceId]["thread"].start()
                captureChanged = True

        # Remove disconnected devices ..
        if len(captureDevices.keys()) > 0:
            freeDeviceKeys = []
            validDevices = [dev.phys for dev in activeDevices]
            for device in list(captureDevices.keys()):
                if device.split('_')[-1] not in validDevices:
                    freeDeviceKeys.append(device)
            for key in freeDeviceKeys:
                captureDevices.pop(key)
                captureChanged = True
        if captureChanged == True:
            logger.info("Capture devices {0}".format(list(captureDevices.keys())))
            for deviceName in captureDevices.keys():
                logger.info(" - {0} = {1}".format(deviceName, captureDevices[deviceName]["name"]))

        time.sleep(3)
    except:
        logger.warning(" - Failed to update device list!")
        traceback.print_exception(*sys.exc_info())
        sys.exit(1)
