FROM mxklb/py-docker:2.0.0 as builder

RUN apt-get update && apt-get install -y gcc git

# Build wheel from source
RUN git clone -b v1.9.1 https://github.com/gvalkov/python-evdev.git evdev
RUN cd evdev && python setup.py bdist_wheel
RUN cp -r evdev/dist /tmp/evdev
RUN rm -rf evdev/

FROM mxklb/py-docker:2.0.0

WORKDIR /app

COPY gitversion /app/gitversion
COPY mqttclient /app/mqttclient
COPY config /app/config

COPY --from=builder /tmp/evdev /tmp/evdev
RUN pip3 install /tmp/evdev/evdev*.whl

COPY *.py ./
RUN chmod +x joystick.py

ENTRYPOINT ["./joystick.py"]