import orjson

# Create dict from given config
def parse_config(config_json):
    config = {}

    has_left_joystick = False
    has_right_joystick = False
    has_joysticks = "joysticks" in config_json.keys()
    if has_joysticks:
        joysticks = config_json["joysticks"].keys()
        has_left_joystick = "left" in joysticks
        has_right_joystick = "right" in joysticks

    # Init button codes (int)
    config["llBtn"] = -294 # Optional second L button
    config["rrBtn"] = -295 # Optional second R button
    config["joyLBtn"] = -298 # Button of left joystick
    config["joyRBtn"] = -299 # Button of right joystick
    config["aBtn"] = config_json["buttons"]["aBtn"]
    config["bBtn"] = config_json["buttons"]["bBtn"]
    config["xBtn"] = config_json["buttons"]["xBtn"]
    config["yBtn"] = config_json["buttons"]["yBtn"]
    config["selBtn"] = config_json["buttons"]["selBtn"]
    config["staBtn"] = config_json["buttons"]["staBtn"]
    config["lBtn"] = config_json["buttons"]["lBtn"]
    config["rBtn"] = config_json["buttons"]["rBtn"]
    if "llBtn" in config_json["buttons"].keys():
        config["llBtn"] = config_json["buttons"]["llBtn"]
    if "rrBtn" in config_json["buttons"].keys():
        config["rrBtn"] = config_json["buttons"]["rrBtn"]
    if "joyLBtn" in config_json["buttons"].keys():
        config["joyLBtn"] = config_json["buttons"]["joyLBtn"]
    if "joyRBtn" in config_json["buttons"].keys():
        config["joyRBtn"] = config_json["buttons"]["joyRBtn"]

    # Init direction button ids (string)
    if "horizontal" in config_json["buttons"].keys():
        config["horizontal"] = config_json["buttons"]["horizontal"]
    if "vertical" in config_json["buttons"].keys():
        config["vertical"] = config_json["buttons"]["vertical"]
    if "up" in config_json["buttons"].keys():
        config["up"] = config_json["buttons"]["up"]
    if "down" in config_json["buttons"].keys():
        config["down"] = config_json["buttons"]["down"]
    if "left" in config_json["buttons"].keys():
        config["left"] = config_json["buttons"]["left"]
    if "right" in config_json["buttons"].keys():
        config["right"] = config_json["buttons"]["right"]

    # Init joystick direction ids
    config["left_vertical"] = "ABS_1234"
    config["left_horizontal"] = "ABS_4321"
    config["right_vertical"] = "ABS_5678"
    config["right_horizontal"] = "ABS_8765"
    if has_left_joystick:
        config["left_vertical"] = config_json["joysticks"]["left"]["vertical"]
        config["left_horizontal"] = config_json["joysticks"]["left"]["horizontal"]
    if has_right_joystick:
        config["right_vertical"] = config_json["joysticks"]["right"]["vertical"]
        config["right_horizontal"] = config_json["joysticks"]["right"]["horizontal"]

    return config

# Container to store joystick config
class Joyfig():
    def __init__(self, path):
        self.path = path
        with open(path) as jsonFile:
            config_json = orjson.loads(jsonFile.read())
        self.name = config_json["name"]
        self.config = parse_config(config_json)