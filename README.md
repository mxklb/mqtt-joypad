# MQTT Joypad

This captures classic NES controller events and publishes them to a MQTT
Broker to the topic `joypad`.

    docker-compose up --build

Note: Input controllers get recognized during runtime. It's plug & play ;)

## Dependencies

Make sure you have a controller plugged to your machine and that it's
recognized by your operating system as */dev/input* device. Further
more prepare your controller configuration as shown below. To successfully
publish controller events via MQTT make sure your MQTT broker is configured
correctly.

Note: Actually the MQTT connection does not configure any authorization!

## Configuration

Each supported device type must defined a `config/*.json` file. The device
`name` is used to grep the input device for capturing. To add support for other
devices, add new `config/*.json` files. In general, the device
signal mapping to button names is defined within these config files.

It is assumed that a joypad has at least the following buttons:

- start & select
- vertical (up/down button)
- horizontal (left/right button)
- left, right (forefinger)
- A, B, X, Y

Additionally/Optionally

- LL, RR (middle finger)
- left_joystick (left thumb)
- right_joystick (right thumb)
- left & right_joystick_press

can be configured. Refer to the existing config files to see how to assign
event codes and ids based on pythons
[evdev](https://python-evdev.readthedocs.io/en/latest/index.html).
The configuration path to be used by the container can be set within the
*.env* file. Additionally configure your MQTT broker to be triggered here.

Note: This is tested with *config/hama-analog.json* - joypad operating
in *analog* mode - on linux desktop & rapsberry pi.
